class UsersController < ApplicationController

  def home
  end

  def new
  	# para creas usuario
  	@user = User.new

  	# valores que vienen de home
  	@username=params[:username]
  	@email=params[:email]
  	@password=params[:password] 

    if @username.blank? or @email.blank? or @password.blank? or not @password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).{6,}$/)
      flash.now[:warning] = "No puedes dejar ningun campo en blanco"
      # si la cotnraseña esta prsente tiene que cumplir las condiciones
      if not @password.blank? and not @password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).{6,}$/)
        flash.now[:warning] = "La contraseña debe tener al menos 6 carácteres, una mayúscula y un dígito"
      end
      render 'home'
    else
      # si el usuario ya esta registrado se emite una viso
      if User.find_by_email(@email)
        flash.now[:warning] = "El email que has introducido ya esta en uso, es posible que ya estes registrado!"
        render 'home'
      elsif User.find_by_username(@username)
        flash.now[:warning] = "El nombre de usuario que has introducido ya esta en uso!"
        render 'home'
      else
        render 'new'
      end
    end 
  end

  def create
    @user = User.new(user_params)
    
    # comprobamos que se guarde bien y que acepte los términos y condiciones
    if @user.save 
      #if params[:acepto]==1
        sign_in @user
        flash.now[:success] = "Bienvenido a PassItEDU "+@user.name+"!"
        redirect_to @user
      #else
      	#@user.errors.add :debes, "aceptar las políticas de privacidad y términos de uso"
      #	render 'new'
      #end
    else
      render 'new'
    end
  end

  def show
  	@user = User.find_by_username(params[:id])
  end

  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation, :username, :birth, :gender, :surname, :studies)
    end
end

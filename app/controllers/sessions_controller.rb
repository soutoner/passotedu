class SessionsController < ApplicationController

	def new
  end

  def create
  	if params[:session][:login].downcase.match(/\A[a-zA-Z0-9]+\Z/).blank?
  		user = User.find_by(email: params[:session][:login].downcase)
  	else
  		user = User.find_by(username: params[:session][:login].downcase)
  	end
  		  	
	  if user && user.authenticate(params[:session][:pass])
	    sign_in user
	    flash.now[:success] = "Bienvenido "+user.name+"!"
      redirect_to user
	  else
	    flash.now[:error] = 'Email/nombre de usuario o contraseña incorrecta en el inicio de sesión' # Not quite right!
      render :template => "users/home" # para renderizar un metodo de otro controlador"
	  end
  end

  def destroy
    sign_out
    redirect_to root_url
  end
end

class User < ActiveRecord::Base
  def to_param
    username # use username instead of id
  end

  has_secure_password
  before_create :create_remember_token

	## BEFORE SAVE
	before_save { email.downcase! }
	before_save { username.downcase! }
  before_save { name.capitalize! }
  before_save { surname.split.map(&:capitalize).join(' ') }

	## VALIDATIONS
	validates :username, presence: true, length: { maximum: 15 },
            format: { with: /\A[a-zA-Z0-9]+\Z/ }, 
            uniqueness: { case_sensitive: false } # ni email ni name pueden ser nil
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(?:\.[a-z\d\-]+)*\.[a-z]+\z/i
  validates :email, presence: true, length: { maximum: 50 },
            format: { with: VALID_EMAIL_REGEX },
            uniqueness: { case_sensitive: false }
  validates :name, presence: true, length: { maximum: 25 },
            format: { with: /\A[a-zA-Z[:alpha:]]+\Z/ } #only letters and tilde
  validates :surname, presence: true, length: { maximum: 50 },
            format: { with: /\A[a-zA-Z[:alpha:]\ ]+\Z/ }
  validates :gender, presence: true, length: { maximum: 1 },
            format: { with: /\A[HM]+\Z/ }
  validates :studies, length: { maximum: 50 },
            format: { with: /\A[a-zA-Z[:alpha:]\ ]+\Z/ }
  validates :birth, presence: true, length: { maximum: 10 },
            format: { with: /\A[0-9\-]+\Z/ }
  validate :old_enough
  # validates :password, length: { minimum: 6 } # validated in password_complexity
  validate :password_complexity



  def password_complexity
    if password.present? and not password.match(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[\d]).{6,}$/)
      errors.add :contraseña, "debe tener al menos 6 carácteres, incluir una mayúscula y un dígito"
    end
  end

  def old_enough
    if 16.years.ago<Date.parse(birth)
      errors.add :debes, "tener más de 16 años"
    end
    rescue ArgumentError
      # catch del error en el que la fecha no sea valida (par apasar test)
  end
  def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  private

    def create_remember_token
      self.remember_token = User.digest(User.new_remember_token)
    end
end
require 'spec_helper'

describe "Static pages" do

  base_title="#YoSoyEDU"

  describe "Home page" do

    it "should have the content 'Bienvenidos'" do
      visit '/'
      expect(page).to have_content('Bienvenidos')
    end

    it "should have the title 'Bienvenidos - #YoSoyEDU'" do
      visit '/'
      expect(page).to have_title(full_title('Bienvenidos')) # full_title definido en spec/static_pages_helper.rb
    end
  end

  describe "Contact page" do

    it "should have the content 'Contacto'" do
      visit '/static_pages/contact'
      expect(page).to have_content('Contacto')
    end

    it "should have the title 'Contacto - #{base_title}'" do
      visit '/static_pages/contact'
      expect(page).to have_title(full_title('Contacto'))
    end
  end

  describe "About page" do

    it "should have the content 'Sobre Nosotros'" do
      visit '/static_pages/about'
      expect(page).to have_content('Sobre Nosotros')
    end

    it "should have the title 'Sobre Nosotros - #{base_title}'" do
      visit '/static_pages/about'
      expect(page).to have_title(full_title('Sobre Nosotros'))
    end
  end

  describe "Policies page" do

    it "should have the content 'Políticas de Privacidad'" do
      visit '/static_pages/policy'
      expect(page).to have_content('Políticas de Privacidad')
    end

    it "should have the title 'Políticas de Privacidad - #{base_title}'" do
      visit '/static_pages/policy'
      expect(page).to have_title(full_title('Políticas de Privacidad'))
    end
  end
  describe "Conditions page" do

    it "should have the content 'Términos de Uso'" do
      visit '/static_pages/cond'
      expect(page).to have_content('Términos de Uso')
    end

    it "should have the title 'Términos de Uso - #{base_title}'" do
      visit '/static_pages/cond'
      expect(page).to have_title(full_title('Términos de Uso'))
    end
  end

end
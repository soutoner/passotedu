require 'spec_helper'

describe "Authentication" do
  subject { page }

  describe "signin page" do
    before { visit signin_path }

    it { should have_content('Iniciar Sesión') }
    it { should have_title('Iniciar Sesión') }
  end
  describe "signin" do
    before { visit signin_path }

    # describe "with invalid information" do
    #   before { click_button "Iniciar Sesión" }

    #   it { should have_title('Bienvenidos') }
    #   it { should have_selector('div.alert') }
    # end
    # describe "with valid information" do
    # 	render 'header'
    #   let(:user) { FactoryGirl.create(:user) }
    #   before do
    #     fill_in :login, with: user.email
    #     fill_in :pass, with: user.password
    #     click_button "Iniciar Sesión"
    #   end

    #   it { should have_title(user.name) }
    #   it { should have_link('Profile',     href: user_path(user)) }
    #   it { should have_link('Sign out',    href: signout_path) }
    #   it { should_not have_link('Sign in', href: signin_path) }
    # end
  end
end
require 'spec_helper'

describe "User pages" do

  subject { page }

  describe "Bienvenidos page" do
    before { visit unete_path }

    it { should have_content('Únete') }
    it { should have_title(full_title('Bienvenidos')) }
  
    describe "introduce info" do

      before { visit unete_path }

      let(:submit) { "Enviar" }

      describe "con informacion insuficiente" do
        it "should render home" do
          click_button submit
          expect(page).to have_title full_title('Bienvenidos')
        end
      end
      describe "con una contraseña poco segura" do
        before do
          fill_in :email, :with => 'user@example.com'
          fill_in :password, :with => 'password'
          fill_in :username, :with => 'username'
        end
        it "should render home" do
          click_button submit
          expect(page).to have_title full_title('Bienvenidos')
          expect(page).to have_selector('div.alert') 
        end
      end
      describe "con informacion valida" do
        before do
          fill_in :email, :with => 'user@example.com'
          fill_in :password, :with => 'passWORD6'
          fill_in :username, :with => 'username'
        end
        it "should render new" do
          click_button submit
          expect(page).to have_title full_title('Únete')
        end
      end
      describe "usuario con mail registrado" do
        let(:user) { FactoryGirl.create(:user) }
        before do
          fill_in :email, :with => user.email
          fill_in :password, :with => 'passWORD6'
          fill_in :username, :with => 'elmichael'
        end
        it "should render home" do 
          click_button submit
          expect(page).to have_title full_title('Bienvenidos')
          expect(page).to have_selector('div.alert')
        end
      end
      describe "username repetido" do
        let(:user) { FactoryGirl.create(:user) }
        before do
          fill_in :email, :with => 'uno@example.com'
          fill_in :password, :with => 'passWORD6'
          fill_in :username, :with => user.username
        end
        it "should render home" do
          click_button submit
          expect(page).to have_title full_title('Bienvenidos')
          expect(page).to have_selector('div.alert')
        end
      end
    end
  end

  ## TODO TEST DE Únete (users/new)
  
  describe "Perfil page" do
	  let(:user) { FactoryGirl.create(:user) } # definido en spec/factories.rb
	  before { visit user_path(user) }

	  it { should have_content(user.username) }
	  it { should have_title(user.username) }
	end
end
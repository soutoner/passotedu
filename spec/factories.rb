FactoryGirl.define do
  factory :user do
    username     "elmichael"
    email    "uno@example.com"
    password "foobaR6"
    password_confirmation "foobaR6"
    name "michael"
    surname "gacela tomson"
    studies "turismo"
    birth	"1994-08-13"
    gender "H"
  end
end
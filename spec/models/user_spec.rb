require 'spec_helper'

describe User do

  before do
   	@user = User.new(username: "example", email: "user@example.com",
                     password: "fooBar6", password_confirmation: "fooBar6", name: "michael",
                     surname: "gacela tomson", studies: "turismo", birth: "1994-08-13",
                     gender: "H")
	end

  subject { @user }

  ## BASIC TESTS
  it { should respond_to(:username) }
  it { should respond_to(:email) }
  it { should respond_to(:password_digest) }
  it { should respond_to(:password) }
  it { should respond_to(:password_confirmation) }
  it { should respond_to(:authenticate) }
  it { should respond_to(:name) }
  it { should respond_to(:surname) }
  it { should respond_to(:studies) }
  it { should respond_to(:birth) }
  it { should respond_to(:gender) }


  it { should be_valid }

  ## USERNAME TESTS
  describe "when username is not present" do
    before { @user.username = " " }
    it { should_not be_valid }
  end
  describe "when username is too long" do
    before { @user.username = "a" * 51 }
    it { should_not be_valid }
  end
  describe "when username has whitespaces" do
    before { @user.username = "ola k ase"}
    it { should_not be_valid }
  end
  describe "when username is already taken" do
    before do
      user_with_same_username = User.new(username: "example", email: "otro@example.com") 
      user_with_same_username.username = user_with_same_username.username.downcase
      user_with_same_username.save

    	it { should_not be_valid }
    end
  end

  ## EMAIL TESTS
  describe "when email is not present" do
    before { @user.email = " " }
    it { should_not be_valid }
  end
  describe "when email format is invalid" do
    it "should be invalid" do
      addresses = %w[user@foo,com user_at_foo.org example.user@foo.
                     foo@bar_baz.com foo@bar+baz.com]
      addresses.each do |invalid_address|
        @user.email = invalid_address
        expect(@user).not_to be_valid
      end
    end
  end
  describe "when email format is valid" do
    it "should be valid" do
      addresses = %w[user@foo.COM A_US-ER@f.b.org frst.lst@foo.jp a+b@baz.cn]
      addresses.each do |valid_address|
        @user.email = valid_address
        expect(@user).to be_valid
      end
    end
  end
  describe "when email address is already taken" do
    before do
      user_with_same_email = @user.dup
      user_with_same_email.email = @user.email.upcase
      user_with_same_email.save
    end

    it { should_not be_valid }
  end
  describe "email address with mixed case" do
    let(:mixed_case_email) { "Foo@ExAMPle.CoM" }

    it "should be saved as all lower-case" do
      @user.email = mixed_case_email
      @user.save
      expect(@user.reload.email).to eq mixed_case_email.downcase
    end
  end

  ## NAME AND SURNAME
  describe "when name is not present" do
    before { @user.name = " " }
    it { should_not be_valid }
  end
  describe "when name is too long" do
    before { @user.name = 'a'*51 }
    it { should_not be_valid }
  end
  describe "when name has whitespaces" do
    before { @user.name = "ola k ase"}
    it { should_not be_valid }
  end
  describe "when name has numbers" do
    before { @user.name = "ola5"}
    it { should_not be_valid }
  end
  describe "when surname is not present" do
    before { @user.surname = " " }
    it { should_not be_valid }
  end
  describe "when surname is too long" do
    before { @user.surname = 'a'*51 }
    it { should_not be_valid }
  end
  describe "when surname has numbers" do
    before { @user.surname = "El ola5"}
    it { should_not be_valid }
  end

  ## GENDER
  describe "when gender is not present" do
    before { @user.gender = ""}
    it { should_not be_valid }
  end
  describe "when gender is unknown" do
    before { @user.gender = "P"}
    it { should_not be_valid }
  end
  describe "when gender is long" do
    before { @user.gender = "PP"}
    it { should_not be_valid }
  end

  ## STUDIES
  describe "when studies is not present" do
    before { @user.studies = " " }
    it { should be_valid }
  end
  describe "when studies is too long" do
    before { @user.studies = 'a'*51 }
    it { should_not be_valid }
  end
  describe "when studies has whitespaces" do
    before { @user.studies = "ola k ase"}
    it { should be_valid }
  end
  describe "when studies has numbers" do
    before { @user.studies = "ola5"}
    it { should_not be_valid }
  end

  ##BIRTH
  describe "when birth is not present" do
    before { @user.birth = "" }
    it { should_not be_valid }
  end
  describe "when birth is in a strange format" do
    before { @user.birth = "2010/30/12" }
    it { should_not be_valid }
  end
  describe "when is not old enough" do
    before { @user.birth = "2014-01-01" }
    it { should_not be_valid }
  end

  ## PASSWORD TESTS
  describe "when password is not present" do
	  before do
	    @user = User.new(username: "Example", email: "user@example.com",
	                     password: " ", password_confirmation: " ", birth: "1994-08-13")
	  end
	  it { should_not be_valid }
	end
	describe "when password doesn't match confirmation" do
	  before { @user.password_confirmation = "mismatch" }
	  it { should_not be_valid }
	end
	describe "with a password that's too short" do
		before { @user.password = @user.password_confirmation = "a" * 5 }
		it { should be_invalid }
	end
	describe "when password is not secure" do
	  before do
	    @user = User.new(username: "Example", email: "user@example.com",
	                     password: "foo", password_confirmation: "foo", birth: "1994-08-13")
	  end
	  it { should_not be_valid }
	end

	## AUTHENTICATE TESTS
	describe "return value of authenticate method" do
	  before { @user.save }
	  let(:found_user) { User.find_by(email: @user.email) }

	  describe "with valid password" do
	    it { should eq found_user.authenticate(@user.password) }
	  end

	  describe "with invalid password" do
	    let(:user_for_invalid_password) { found_user.authenticate("invalid") }

	    it { should_not eq user_for_invalid_password }
	    specify { expect(user_for_invalid_password).to be_false }
	  end
	end

end
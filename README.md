README

Si nos encontramos SampleApp hay que cambiarlo por -> Web

# Full_title

Establece un titulo base y añade el titulo que le da 'provide'

Esta en app/helpers/application_helper.rb

MODELOS:

* Categoría (nombre, desc. corta | apuntes)
* Apunte (nombre, desc. corta, categoria_id | usuarios_who_faved)

FUENTES

* Lora para textos grandes y apuntes (serif)
* Roboto para el resto de la pagina

TODO:
* Mantener datos en form si me equivoco en el formulario
* Avatares por defecto para gente sin avatar
* Problemas acepto temrinos y condiciones
* YTest y finalizar sign in
* TEST users/new (únete)
* Implementar actividad (quien sigue a quien, quien ha favoriteado que) (https://github.com/pokonski/public_activity)
* Mensajeria privada (https://github.com/mailboxer/mailboxer)
* Búsquedas


CSS:
* Para zonas de texto sobre fondo oscuro poner clase "dark"

WORKFLOW
* rails generate controller EjemploNombre metodo1 metodo2 ...
* rails generate integration_test ejemplo_nombre
* Escribir codigo

OJO!:
* No olvidar eliminar de layouts/aplication.html.erb la parte de debug

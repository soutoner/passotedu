class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :username
      t.string :email
      t.string :password_digest
      t.string :name
      t.string :surname
      t.string :studies
      t.string :gender
      t.string :birth
      t.string :remember_token

      t.timestamps
    end
  	add_index :users, :email
    add_index  :users, :remember_token
    add_index  :users, :username
  end
end

Rails.application.routes.draw do
  require File.expand_path("../../lib/logged_in_constraint", __FILE__)

  get '/' => 'users#home', :constraints => LoggedInConstraint.new(false) # para usuario no logueado
  root :to => 'static_pages#contact', :constraints => LoggedInConstraint.new(true) # para usuario logueado

  match '/unete',  to: 'users#new',            via: 'get'
  match '/condiciones',    to: 'static_pages#cond',    via: 'get'
  match '/politicas',    to: 'static_pages#policy',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get'
  match '/contacto', to: 'static_pages#contact', via: 'get'
  match '/salir', to: 'sessions#destroy',     via: 'delete'

  # necesario para que el home publique en el registro
  resources :users, :collection=>{:home => :get, :new => :post}
  post 'users/new'

  resources :sessions, only: [:new, :create, :destroy]

  # get 'static_pages/home'
  get 'static_pages/about'
  get 'static_pages/contact'
  get 'static_pages/policy'
  get 'static_pages/cond'

  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
